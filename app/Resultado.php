<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resultado extends Model
{
    protected $table = 'toma_muestras_resultados';
    public $timestamps = false;
    protected $fillable = ['descargas'];
    protected $primaryKey = 'id_muestra';
}
