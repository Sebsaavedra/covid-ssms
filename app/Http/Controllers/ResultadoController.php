<?php

namespace App\Http\Controllers;

use App\Consulta;
use App\Resultado;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class ResultadoController extends Controller
{
    public function request_pdf($workEffortId = null, $caseId = null)
    {
	//$workEffortId = 182136;
	//$caseId = 193122;
	$date = date('d-m-Y');

        $curl = curl_init();
	$params=["caseTestId"=> $caseId, "workEffortId"=>$workEffortId, "pdfDownloaded"=>true, "pdfDownloadDate" => $date, "testResultsNotificationDate" => $date];
        curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://tta.moit.cl/rest/s1/cvdcntrl/cases/$workEffortId/tests/$caseId",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => '',
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => 'PATCH',
	  CURLOPT_POSTFIELDS => json_encode($params),
	  CURLOPT_HTTPHEADER => array(
	    'Content-Type: application/json',
	    'Accept: application/json',
	    'Authorization: Basic bXJvamFzQGFnaWxpdGljLmNsOmhvbGFob2xhIzI=',
	    'Cookie: moqui.visitor=4233ecb9-7fbf-425e-918c-78e0252b0b69; route=1613068756.856.39.333687; JSESSIONID=node03xyq75poh8jb5m5ll9ha0u135449.node0; hazelcast.sessionId=HZ214C72C606424A8FB550458D2A3F3A7F'
	  ),
	));
	$response = curl_exec($curl);
	curl_close($curl);
	//echo $response;
    }

    public function findByRut(Request $request)
    {
        $request->validate([
            'rut' => 'alpha_dash|required',
            'telefono' => 'numeric|required|digits:9',
            'calle' => 'string|required',
            'numero' => 'string|required',
            'comuna' => 'string|required'
        ]);

        $consulta = new Consulta();
        $consulta->fill($request->all());
        if ($consulta->save()) {
            $resultado = Resultado::where('id_paciente', $request->rut)
            ->where('id_muestra', $request->id_laboratorio)
            ->where('resultado', 'Negativo')
            ->first();

            return view('resultado', compact('resultado'));
        }
    }

    public function generar_pdf($orden)
    {
        $resultado = Resultado::where('id_muestra', $orden)
            ->where('resultado', 'Negativo')
            ->first();

        $pdf = PDF::loadView('pdf', compact('resultado'));

        return $pdf->stream();
    }

     public function generar_pdf_post(Request $request)
    {
	 $resultado = Resultado::where('id_muestra', $request->orden)
            ->where('resultado', 'Negativo')
            ->first();

        $pdf = PDF::loadView('pdf', compact('resultado'));

	$resultado->descargas = $resultado->descargas +1;
	$resultado->update();

	$this->request_pdf($resultado->workEffortId, $resultado->caseTestId);

        return $pdf->download('certificado.pdf');

    }
}
