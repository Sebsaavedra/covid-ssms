<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consulta extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rut', 'id_laboratorio','telefono', 'calle', 'numero', 'depto', 'comuna', 'email'
    ];
}
